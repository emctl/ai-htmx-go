//go:build !dev
// +build !dev

package main

import (
	"embed"
	"log/slog"
	"net/http"
)

// This package allows for tailwindcss rebuild the css without rebuilding the whole binary.

//go:embed public
var publicFS embed.FS

func public() http.Handler {
	slog.Debug("Serving public in prod")
	return http.FileServerFS(publicFS)
}
