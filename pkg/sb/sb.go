package sb

import (
	"log/slog"
	"os"

	"github.com/nedpals/supabase-go"
)

const ResetPasswordEndpoint = "/auth/v1/recover"

var BaseAuthURL = "https://abtqdwvyqiptoqmydqor.supabase.co" + ResetPasswordEndpoint

var Client *supabase.Client

func Init() error {
	sbHost := os.Getenv("SUPABASE_URL")
	sbSecret := os.Getenv("SUPABASE_SECRET_KEY")
	Client = supabase.CreateClient(sbHost, sbSecret)
	slog.Info("Supabase client initialized")
	return nil
}
