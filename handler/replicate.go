package handler

import (
	"ai-htmx-go/db"
	"ai-htmx-go/types"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/uptrace/bun"
)

const (
	success    = "succeded"
	processing = "processing"
)

type ReplicateResp struct {
	Input struct {
		Prompt string `json:"prompt"`
	} `json:"input"`
	Status string   `json:"status"`
	Output []string `json:"output"`
}

func HandleReplicateCallback(w http.ResponseWriter, r *http.Request) error {
	var resp ReplicateResp
	if err := json.NewDecoder(r.Body).Decode(&resp); err != nil {
		return err
	}
	if resp.Status == processing {
		return nil
	}
	if resp.Status != success {
		return fmt.Errorf("replication failed: %v", resp.Output)
	}
	batchID, err := uuid.Parse(chi.URLParam(r, "batch_id"))
	if err != nil {
		return fmt.Errorf("invalid batch id: %s", err)
	}
	images, err := db.GetImagesByBatchID(batchID)
	if err != nil {
		return fmt.Errorf("failed to get images by batch id: %s, %s", batchID, err)
	}

	if len(images) != len(resp.Output) {
		return fmt.Errorf("replication callback failed: %v, different value of images to batch total", resp.Output)
	}

	err = db.Bun.RunInTx(r.Context(), &sql.TxOptions{}, func(ctx context.Context, tx bun.Tx) error {
		for i, imageURL := range resp.Output {
			images[i].Status = types.ImageStatusCompleted
			images[i].ImageLocation = imageURL
			images[i].Prompt = resp.Input.Prompt
			if err := db.UpdateImage(r.Context(), &images[i]); err != nil {
				return fmt.Errorf("failed to update image: %s", err)
			}

		}
		return nil
	})
	return err
}
