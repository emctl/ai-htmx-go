package handler

import (
	"ai-htmx-go/db"
	"ai-htmx-go/pkg/kit/validate"
	"ai-htmx-go/pkg/sb"
	"ai-htmx-go/types"
	"ai-htmx-go/view/auth"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"

	"github.com/gorilla/sessions"

	"github.com/nedpals/supabase-go"
)

// var appLocation = "http://" + os.Getenv("HTTP_LISTEN_IP") + ":" + os.Getenv("HTTP_LISTEN_ADDR")

const (
	sessionUserKey        = "user"
	sessionAccessTokenKey = "sb_access_token"
)

func HandleResetPasswordIndex(w http.ResponseWriter, r *http.Request) error {
	return render(r, w, auth.ResetPassword())
}

func HandleResetPasswordCreate(w http.ResponseWriter, r *http.Request) error {
	fmt.Println("Reset password create")
	user := getAuthenticatedUser(r)
	params := map[string]string{
		"email":      user.Email,
		"redirectTo": "http://localhost:3000/auth/reset-password",
	}
	b, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s", sb.BaseAuthURL), bytes.NewReader(b))
	req.Header.Set("apikey", os.Getenv("SUPABASE_SECRET_KEY"))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		b, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		return fmt.Errorf("error sending reset password request: %d => %s", resp.StatusCode, string(b))
	}

	return render(r, w, auth.ResetPasswordInitiated(user.Email))
}

func HandleResetPasswordUpdate(w http.ResponseWriter, r *http.Request) error {
	user := getAuthenticatedUser(r)
	params := map[string]any{
		"password": r.FormValue("password"),
	}
	_, err := sb.Client.Auth.UpdateUser(r.Context(), user.AccessToken, params)
	errors := auth.ResetPasswordErrors{
		NewPassword: "Please enter a valid password."}
	if err != nil {
		return render(r, w, auth.ResetPasswordForm(errors))
	}
	return hxRedirect(w, r, "/")
}

func HandleAccountSetupIndex(w http.ResponseWriter, r *http.Request) error {
	return render(r, w, auth.AccountSetup())
}

func HandleAccountSetupCreate(w http.ResponseWriter, r *http.Request) error {
	params := auth.AccountSetupParams{
		Username: r.FormValue("username"),
	}
	var errors auth.AccountSetupErrors
	ok := validate.New(&params, validate.Fields{
		"Username": validate.Rules(validate.Min(2), validate.Max(50)),
	}).Validate(&errors)
	if !ok {
		return render(r, w, auth.AccountSetupForm(params, errors))
	}
	user := getAuthenticatedUser(r)
	account := types.Account{
		UserID:   user.ID,
		Username: params.Username,
	}
	ctx := r.Context()
	if err := db.CreateAccount(ctx, &account); err != nil {
		return err
	}

	return hxRedirect(w, r, "/")
}

func HandleLoginIndex(w http.ResponseWriter, r *http.Request) error {
	return auth.Login().Render(r.Context(), w)
}

func HandleSignupIndex(w http.ResponseWriter, r *http.Request) error {
	return auth.Signup().Render(r.Context(), w)
}

func HandleSignupCreate(w http.ResponseWriter, r *http.Request) error {
	params := auth.SignupParams{
		Email:           r.FormValue("email"),
		Password:        r.FormValue("password"),
		ConfirmPassword: r.FormValue("confirmPassword"),
	}
	errors := auth.SignupErrors{}
	if ok := validate.New(&params, validate.Fields{
		"Email":    validate.Rules(validate.Email),
		"Password": validate.Rules(validate.Password),
		"ConfirmPassword": validate.Rules(
			validate.Equal(params.Password),
			validate.Message("The passwords do not match."),
		),
	}).Validate(&errors); !ok {
		return render(r, w, auth.SignupForm(params, errors))
	}
	slog.Info("Creating user", "email", params.Email)
	user, err := sb.Client.Auth.SignUp(r.Context(), supabase.UserCredentials{
		Email:    params.Email,
		Password: params.Password,
	})
	if err != nil {
		return err
	}
	return render(r, w, auth.SignupSuccess(user.Email))
}

func HandleLoginCreate(w http.ResponseWriter, r *http.Request) error {
	credentials := supabase.UserCredentials{
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	resp, err := sb.Client.Auth.SignIn(r.Context(), credentials)
	if err != nil {
		return render(r, w, auth.LoginForm(credentials, auth.LoginErrors{
			InvalidCreds: "The email or password is incorrect. Please try again.",
		}))
	}
	store := sessions.NewCookieStore([]byte(os.Getenv("SESSION_SECRET")))
	session, _ := store.Get(r, sessionUserKey)
	session.Values["sb_access_token"] = resp.AccessToken
	if err := session.Save(r, w); err != nil {
		return err
	}
	if err := setAuthSession(w, r, resp.AccessToken); err != nil {
		return err
	}
	return hxRedirect(w, r, "/")
}

func HandleAuthCallback(w http.ResponseWriter, r *http.Request) error {
	accessToken := r.URL.Query().Get("access_token")
	if len(accessToken) == 0 {
		return render(r, w, auth.CallbackScript())
	}
	slog.Debug("Access token received", "token", accessToken)
	if err := setAuthSession(w, r, accessToken); err != nil {
		return err
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
	return nil
}

func HandleLogoutCreate(w http.ResponseWriter, r *http.Request) error {

	store := sessions.NewCookieStore([]byte(os.Getenv("SESSION_SECRET")))
	session, _ := store.Get(r, sessionUserKey)
	session.Values[sessionAccessTokenKey] = ""
	session.Save(r, w)
	http.Redirect(w, r, "/", http.StatusSeeOther)
	return nil
}

func setAuthSession(w http.ResponseWriter, r *http.Request, token string) error {
	store := sessions.NewCookieStore([]byte(os.Getenv("SESSION_SECRET")))
	session, _ := store.Get(r, sessionUserKey)
	session.Values[sessionAccessTokenKey] = token
	return session.Save(r, w)
}

func HandleLoginWithGoogleCreate(w http.ResponseWriter, r *http.Request) error {
	resp, err := sb.Client.Auth.SignInWithProvider(supabase.ProviderSignInOptions{
		Provider:   "google",
		RedirectTo: fmt.Sprintf("http://%s:%s/auth/callback", os.Getenv("HTTP_LISTEN_IP"), os.Getenv("HTTP_LISTEN_ADDR")),
	})
	if err != nil {
		slog.Error("Error signing in with Google", "error", err)
		return err
	}
	slog.Debug("Redirecting to Google", "url", resp.URL)
	http.Redirect(w, r, resp.URL, http.StatusSeeOther)
	return nil
}
