package handler

import (
	"ai-htmx-go/db"
	"ai-htmx-go/pkg/sb"
	"ai-htmx-go/types"
	"context"
	"database/sql"
	"errors"
	"log/slog"
	"net/http"
	"os"
	"strings"

	"github.com/google/uuid"
	"github.com/gorilla/sessions"
)

func WithAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			if strings.Contains(r.URL.Path, "/public") {
				next.ServeHTTP(w, r)
				return
			}
			user := getAuthenticatedUser(r)
			if !user.LoggedIn {
				slog.Debug("User not authenticated")
				http.Redirect(w, r, "/login", http.StatusSeeOther)
				return
			}
			next.ServeHTTP(w, r)
		})
}

func WithAccountSetup(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			user := getAuthenticatedUser(r)
			account, err := db.GetAccountByUserID(user.ID)
			// The user has not setup their account yet
			// Hence, redirect them to the account setup page
			if err != nil {
				if errors.Is(err, sql.ErrNoRows) {
					http.Redirect(w, r, "/account/setup", http.StatusSeeOther)
					return
				}
				next.ServeHTTP(w, r)
				return
			}

			user.Account = account
			ctx := context.WithValue(r.Context(), types.UserContextKey, user)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
}

func WithUser(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			if strings.Contains(r.URL.Path, "/public") {
				next.ServeHTTP(w, r)
				return
			}

			sessionSecret := os.Getenv("SESSION_SECRET")
			if sessionSecret == "" {
				slog.Error("SESSION_SECRET is not set")
			}
			store := sessions.NewCookieStore([]byte(sessionSecret))
			session, err := store.Get(r, sessionUserKey)
			if err != nil {
				slog.Debug("No access token found")
				next.ServeHTTP(w, r)
				return
			}
			accessToken := session.Values[sessionAccessTokenKey]
			if accessToken == nil {
				slog.Debug("No access token found")
				next.ServeHTTP(w, r)
				return
			}
			resp, err := sb.Client.Auth.User(r.Context(), accessToken.(string))
			if err != nil {
				next.ServeHTTP(w, r)
				return
			}

			user := types.AuthenticatedUser{
				ID:          uuid.MustParse(resp.ID),
				Email:       resp.Email,
				LoggedIn:    true,
				AccessToken: accessToken.(string),
			}

			slog.Debug("Checking user authentication")
			ctx := context.WithValue(r.Context(), types.UserContextKey, user)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
}
