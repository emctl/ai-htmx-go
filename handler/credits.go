package handler

import (
	"ai-htmx-go/view/credits"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/stripe/stripe-go/v76"
	"github.com/stripe/stripe-go/v76/checkout/session"
)

func HandleCreditsIndex(w http.ResponseWriter, r *http.Request) error {
	return render(r, w, credits.Index())
}

func HandleStripeCheckoutCreate(w http.ResponseWriter, r *http.Request) error {
	stripe.Key = os.Getenv("STRIPE_API_KEY")
	// fmt.Println(fmt.Sprintf("http://%s:%s/checkout/success", os.Getenv("HOST"), os.Getenv("PORT")))
	checkoutParams := &stripe.CheckoutSessionParams{
		SuccessURL: stripe.String("http://localhost:3000/checkout/success/{CHECKOUT_SESSION_ID}"),
		CancelURL:  stripe.String("http://localhost:3000/checkout/cancel"),
		LineItems: []*stripe.CheckoutSessionLineItemParams{
			{
				Price:    stripe.String(chi.URLParam(r, "PriceID")),
				Quantity: stripe.Int64(1),
			},
		},
		Mode: stripe.String(string(stripe.CheckoutSessionModePayment)),
	}
	session, err := session.New(checkoutParams)
	if err != nil {
		return err
	}
	return hxRedirect(w, r, session.URL)
}

func HandleStripeCheckoutSuccess(w http.ResponseWriter, r *http.Request) error {
	sessionID := chi.URLParam(r, "sessionID")
	stripe.Key = os.Getenv("STRIPE_API_KEY")
	session, err := session.Get(sessionID, nil)
	if err != nil {
		return err
	}
	priceID := session.DisplayItems[0].Price.ID
	switch priceID {
	case os.Getenv("STRIPE_PRICE_100"):
	case os.Getenv("STRIPE_PRICE_250"):
	case os.Getenv("STRIPE_PRICE_550"):
	default:
		return fmt.ErrorF("Invalid price ID")
	}
	return render(r, w, credits.Success(session))
}

func HandleStripeCheckoutCancel(w http.ResponseWriter, r *http.Request) error {
	return nil
}
