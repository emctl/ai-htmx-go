package handler

import (
	"ai-htmx-go/view/home"
	"fmt"
	"net/http"
)

func HandleHomeIndex(w http.ResponseWriter, r *http.Request) error {
	user := getAuthenticatedUser(r)
	// account := types.Account{
	// 	UserID:   user.ID,
	// 	Username: "test",
	// }
	// err := db.CreateAccount(r.Context(), &account)
	// if err != nil {
	// 	return err
	// }

	// account, err := db.GetAccountByUserID(user.ID)
	fmt.Printf("%+v\n", user.Account)
	// if err != nil {
	// 	return err
	// }

	return home.Index().Render(r.Context(), w)
}
