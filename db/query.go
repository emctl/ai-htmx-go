package db

import (
	"ai-htmx-go/types"
	"context"

	"github.com/google/uuid"
)

func CreateImage(ctx context.Context, image *types.Image) error {
	_, err := Bun.NewInsert().
		Model(image).
		Exec(ctx)
	return err
}

func UpdateImage(ctx context.Context, image *types.Image) error {
	_, err := Bun.NewUpdate().
		Model(image).
		WherePK().
		Exec(ctx)
	return err
}

func GetImagesByBatchID(batchID uuid.UUID) ([]types.Image, error) {
	var images []types.Image
	err := Bun.NewSelect().
		Model(&images).
		// Where("deleted = ?", false).
		Where("batch_id = ?", batchID).
		// Order("created_at DESC").
		Scan(context.Background())
	return images, err
}

func GetImagesByUserID(userID uuid.UUID) ([]types.Image, error) {
	var images []types.Image
	err := Bun.NewSelect().
		Model(&images).
		Where("deleted = ?", false).
		Where("user_id = ?", userID).
		Order("created_at DESC").
		Scan(context.Background())
	return images, err
}

func GetImagesByID(id int) (types.Image, error) {
	var image types.Image
	err := Bun.NewSelect().
		Model(&image).
		Where("id = ?", id).
		Scan(context.Background())
	return image, err
}

func UpdateAccount(ctx context.Context, account *types.Account) error {
	_, err := Bun.NewUpdate().
		Model(account).
		WherePK().
		Exec(ctx)
	return err
}

func GetAccountByUserID(userID uuid.UUID) (types.Account, error) {
	var account = types.Account{}
	err := Bun.NewSelect().
		Model(&account).
		Where("user_id = ?", userID).
		Scan(context.Background())
	return account, err
}

func CreateAccount(ctx context.Context, account *types.Account) error {
	_, err := Bun.NewInsert().
		Model(account).
		Exec(ctx)
	return err
}
