package main

import (
	"ai-htmx-go/db"
	"ai-htmx-go/handler"
	"ai-htmx-go/pkg/sb"

	"embed"
	"log"
	"log/slog"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
)

//go:embed public
var FS embed.FS

func main() {
	// This is a placeholder for the main function
	if err := initEverything(); err != nil {
		log.Fatal(err)
	}

	router := chi.NewMux()
	router.Use(handler.WithUser)

	router.Handle("/*", public())

	router.Get("/login", handler.Make(handler.HandleLoginIndex))
	router.Get("/login/provider/google", handler.Make(handler.HandleLoginWithGoogleCreate))
	router.Get("/signup", handler.Make(handler.HandleSignupIndex))
	router.Get("/auth/callback", handler.Make(handler.HandleAuthCallback))
	router.Get("/", handler.Make(handler.HandleHomeIndex))

	router.Post("/login", handler.Make(handler.HandleLoginCreate))
	router.Post("/signup", handler.Make(handler.HandleSignupCreate))
	router.Post("/logout", handler.Make(handler.HandleLogoutCreate))
	router.Post("/replicate/callback/{userID}/{batchID}", handler.Make(handler.HandleReplicateCallback))

	// router.Get("/checkout/create/{productID}", handler.Make(handler.HandleStripeCheckoutCreate))

	router.Group(func(auth chi.Router) {
		auth.Use(handler.WithAuth)
		auth.Get("/account/setup", handler.Make(handler.HandleAccountSetupIndex))
		auth.Post("/account/setup", handler.Make(handler.HandleAccountSetupCreate))
	})

	router.Group(func(auth chi.Router) {
		auth.Use(handler.WithAuth, handler.WithAccountSetup)
		auth.Get("/settings", handler.Make(handler.HandleSettingsIndex))
		auth.Put("/settings/account/profile", handler.Make(handler.HandleSettingsUsernameUpdate))

		auth.Post("/auth/reset-password", handler.Make(handler.HandleResetPasswordCreate))
		auth.Put("/auth/reset-password", handler.Make(handler.HandleResetPasswordUpdate))
		auth.Get("/auth/reset-password", handler.Make(handler.HandleResetPasswordIndex))

		auth.Get("/generate", handler.Make(handler.HandleGenerateIndex))
		auth.Post("/generate", handler.Make(handler.HandleGenerateCreate))

		auth.Get("/generate/image/status/{id}", handler.Make(handler.HandleGenerateImageStatus))
		auth.Get("/buy-credits", handler.Make(handler.HandleCreditsIndex))
		auth.Get("/checkout/create/{PriceID}", handler.Make(handler.HandleStripeCheckoutCreate))
		router.Get("/checkout/success/{sessionID}", handler.Make(handler.HandleStripeCheckoutSuccess))
		router.Get("/checkout/cancel", handler.Make(handler.HandleStripeCheckoutCancel))
	})

	port := os.Getenv("HTTP_LISTEN_ADDR")
	slog.Info("Server is running", "port", port)
	log.Fatal(http.ListenAndServe(":"+port, router))

}

func initEverything() error {
	if err := godotenv.Load(); err != nil {
		return err
	}
	if err := db.Init(); err != nil {
		return err
	}
	return sb.Init()
}
