create table if not exists images(
	id serial primary key,
	user_id uuid references auth.users,
    status int not null default 1,
    batch_id uuid not null,
    prompt text not null,
    image_location text,
    deleted boolean not null default false,
	deleted_at timestamp,
	created_at timestamp not null default now()
)