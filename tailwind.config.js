/** @type {import ('tailwindcss').Config} */
module.exports = {
  content: ["./**/*templ", "./**/*.go", "./**/*.html"],
  safelist: [],
  plugins: [require("daisyui")],
  daisyui: {
    themes: ["sunset"]
  }
}