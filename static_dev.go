//+build dev
//go:build dev
// +build dev

package main

import (
	"log/slog"
	"net/http"
	"os"
)

// This package allows for tailwindcss rebuild the css without rebuilding the whole binary.

func public() http.Handler {
	slog.Debug("Serving public")
	return http.StripPrefix("/public/", http.FileServerFS(os.DirFS("public")))
}
